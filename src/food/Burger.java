package food;

public class Burger extends Item{

    public Burger(String nameBurger,double price_burger){
        super(nameBurger,price_burger);
    }
    @Override
    public String getNameItem() {
        return super.getNameItem();
    }

    @Override
    public double getPriceItem() {
        return super.getPriceItem();
    }

    @Override
    public void setNameItem(String nameItem) {
        super.setNameItem(nameItem);
    }

    @Override
    public void setPriceItem(double priceItem) {
        super.setPriceItem(priceItem);
    }
}
