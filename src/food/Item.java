package food;

public abstract class Item {
    protected String nameItem;
    protected double priceItem;


    public Item(String nameItem,double priceItem) {
        this.nameItem = nameItem;
        this.priceItem = priceItem;
    }

    public double getPriceItem() {
        return priceItem;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public void setPriceItem(double priceItem) {
        this.priceItem = priceItem;
    }
}