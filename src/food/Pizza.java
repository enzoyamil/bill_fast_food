package food;

public class Pizza extends Item{

    public Pizza(String namePizza, double pricePizza){
        super(namePizza,pricePizza);
    }

    @Override
    public String getNameItem() {
        return super.getNameItem();
    }

    @Override
    public double getPriceItem() {
        return super.getPriceItem();
    }

    @Override
    public void setNameItem(String nameItem) {
        super.setNameItem(nameItem);
    }

    @Override
    public void setPriceItem(double priceItem) {
        super.setPriceItem(priceItem);
    }
}
