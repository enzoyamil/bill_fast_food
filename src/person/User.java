package person;

import java.util.Scanner;

public class User {
    private String name;
    private String nit;

    public User(){}

    public User(String name, String nit){
        this.name = name;
        this.nit = nit;
    }
    public String getName(){
        return name;
    }

    public String getNit() {
        return nit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }
    public void registrarse(){
        Scanner option = new Scanner(System.in);
        System.out.println("Ingrese su Nombre por favor");
        String name = option.next();
        System.out.println("Ingrese su NIT por favor");
        String nit = option.next();
        setName(name);
        setNit(nit);
    }
    public boolean realizarPedido(String estado){
    if(estado.equals("si")){
        return  true;
    }else{return  false;}
    }
}
