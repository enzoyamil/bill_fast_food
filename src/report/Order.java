package report;

import food.Item;
public class Order {
    private Item item;
    private int count;
    private double subPrice;

    public Order(){}

    public Order(Item item , int count){
        this.item = item;
        this.count = count;
        subPrice=item.getPriceItem()*count;
    }

    public int getCount() {
        return count;
    }

    public Item getItem() {
        return item;
    }

    public double getSubPrice() {
        return subPrice;
    }
}
