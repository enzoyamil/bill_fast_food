package report;
import food.*;
import menu.Menu;
import person.User;
import java.util.ArrayList;
import java.util.Scanner;

public class Bill {
    private User user;
    private ArrayList<Order> orders = new ArrayList<Order>();
    private double total;

    public Bill(){
    }
   public Bill(User user,ArrayList<Order> orders){
        this.user = user;
        this.orders = orders;
        this.total= total;
    }
    public void emitirFactura(User user){
       System.out.println("Factura");
       System.out.println("------------------");
        System.out.println("Nombre:"+ user.getName());
        System.out.println("NIT:"+ user.getNit());;
        System.out.println("Pedido:");
       for(int i=0;i<orders.size();i++){
           Item item = orders.get(i).getItem();
           System.out.println(item.getNameItem()+" X"+orders.get(i).getCount()+" "+item.getPriceItem()+"Bs");
       }
        System.out.println("Total a Pagar:"+ calcularTotal());
        System.out.println("------------------");
        System.out.println("Gracias por su compra!");
        Menu menu = new Menu();
    }
    private double calcularTotal(){
       double total = 0;
      for(int i=0;i<orders.size();i++) {
          total = total + orders.get(i).getSubPrice();
      }
      return total;
       }
       private  Order agregarExtra(int pedido,int count){
           Order result = new Order();
        if(pedido==1){
            Extra frenchFries = new Extra("Papas Fritas", 5);
                return  result = new Order(frenchFries,count);
        }else{ return result;}

       }
       public Order registrarOrden(int numeroPedido){
           Order result = new Order();
           Scanner option = new Scanner(System.in);
           Burger burger_Vacuno = new Burger("Hamburguesa Vacuno", 20.5);
           Burger burger_Classic = new Burger("Hamburguesa Clasica", 15.5);
           Burger burger_Vegetarian = new Burger("Hamburguesa Vegetariana", 25.5);
           Pizza pizzaNormal = new Pizza("Pizza Normal",45.0);
           Pizza pizzaFamiliar = new Pizza("Pizza Familiar",45.0);
           Pizza pizzaInterminable = new Pizza("Pizza Interminable",45.0);
           Wings wings = new Wings("Alitas",7);
           if (numeroPedido == 1) {
               System.out.println("Que tipo de hamburguesa?");
               System.out.println("Escriba el nuemero de la opción que desea");
               System.out.println("1)Hamburguesa Vacuno");
               System.out.println("2)Hamburguesa Clasica");
               System.out.println("3)Hamburguesa Vegetariana");
               String type = option.next();
               System.out.println("Cuantas hamburguesas?");
               int count = option.nextInt();
               System.out.println("Quiere extras en su Hambueguesa? si o no");
               String extra = option.next();
               switch (type) {
                   case "1":
                       result = new Order(burger_Vacuno, count);
                       if(extra.equals("si")){
                           orders.add(agregarExtra(numeroPedido,count));
                       }else{break;}
                       break;
                   case "2":
                       result = new Order(burger_Classic, count);
                       if(extra.equals("si")){
                           orders.add(agregarExtra(numeroPedido,count));
                       }else{break;}
                       break;
                   case "3":
                       result = new Order(burger_Vegetarian, count);
                       if(extra.equals("si")){
                           orders.add(agregarExtra(numeroPedido,count));
                       }else{break;}
                       break;
                   default:
                       break;
               }

           }else if(numeroPedido==2){
               System.out.println("Que tipo de Pizza?");
               System.out.println("Escriba el nuemero de la opción que desea");
               System.out.println("1)Pizza Normal");
               System.out.println("2)Pizza Familiar");
               System.out.println("3)Pizza Interminable");
               String type = option.next();
               System.out.println("Cuantas pizzas?");
               int count = option.nextInt();
               switch (type) {
                   case "1":
                       result = new Order(pizzaNormal, count);
                       break;
                   case "2":
                       result = new Order(pizzaFamiliar, count);
                       break;
                   case "3":
                       result = new Order(pizzaInterminable, count);
                       break;
                   default:
                       break;
               }
           }else if(numeroPedido==3){
               System.out.println("Cuantas Alitas?");
               int count = option.nextInt();
               result = new Order(wings, count);
           }else{
               Menu menu = new Menu();
           }

           orders.add(result);
           return result;
       }

    }